package br.com.thiagogbferreira.microcalc.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

public class CalcServiceTest {
  private MockRestServiceServer mockRestServiceServer;
  private RestTemplate restTemplate = new RestTemplate();
  private CalcServiceApplication csc;
  private MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
  private MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
  
  @Before
  public void setUp() throws Exception {
    mockHttpServletRequest.setLocalAddr("127.0.0.1");
    mockHttpServletRequest.setLocalName("localhost");
    
    csc = new CalcServiceApplication();
    ReflectionTestUtils.setField(csc, "restTemplate", restTemplate);
    ReflectionTestUtils.setField(csc, "request", mockHttpServletRequest);
    ReflectionTestUtils.setField(csc, "response", mockHttpServletResponse);
    mockRestServiceServer = MockRestServiceServer.createServer(restTemplate);
  }
  
  @Test
  public void needToCallTranslateServiceAndAfterTheOperationServiceWithReturnedByTranslateService() {
    mockRestServiceServer
      .expect(requestTo("http://MICROCALC-TRANSLATE-SERVICE/test"))
      .andExpect(method(HttpMethod.GET))
      .andRespond(withSuccess("OP", MediaType.APPLICATION_JSON));
    
    mockRestServiceServer
      .expect(requestTo("http://MICROCALC-OP-OPERATION/1,2,3"))
      .andExpect(method(HttpMethod.GET))
      .andRespond(withSuccess("6", MediaType.APPLICATION_JSON));

    ResponseEntity<BigDecimal> ret = csc.calc("test", new BigDecimal("1"), new BigDecimal("2"), new BigDecimal("3"));
    assertThat(ret.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(ret.getBody()).isEqualTo(new BigDecimal("6"));
  }
  
}

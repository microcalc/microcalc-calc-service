package br.com.thiagogbferreira.microcalc.service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


/**
 * @author Thiago Ferreira
 *
 */
@SpringBootApplication
@RestController
public class CalcServiceApplication {
  public static final String OPERATION_URL = "http://MICROCALC-%1s-OPERATION/{param}";
  public static final String TRANSLATE_URL = "http://MICROCALC-TRANSLATE-SERVICE/{param}";

  private RestTemplate restTemplate = new RestTemplate();

  @Autowired
  private HttpServletRequest request;
  @Autowired
  private HttpServletResponse response;

  
  /**
   * @param args
   */
  public static void main(String[] args) {
    SpringApplication.run(CalcServiceApplication.class, args);
  }
  
  /**
   * @param param
   * @return
   */
  @RequestMapping("/{alias}/{params:.+}")
  public ResponseEntity<BigDecimal> calc(
      @PathVariable String alias, 
      @PathVariable BigDecimal... params
  ) {
    response.addHeader("Local-Addr", request.getLocalAddr());
    response.addHeader("Local-Name", request.getLocalName());
    String operation = restTemplate.getForEntity(TRANSLATE_URL, String.class, alias).getBody();
    
    String paramsStr = Arrays.stream(params)
          .map(n -> n.toPlainString().trim())
          .collect(Collectors.joining(","));
    
    return 
        ResponseEntity
          .ok(restTemplate.getForEntity(
                  String.format(OPERATION_URL, operation)
                  , BigDecimal.class
                  , paramsStr
                )
             )
          .getBody();
  }
  
}
